Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1991, 1999, Free Software Foundation, Inc.
 1989, 1991, Free Software Foundation, Inc.
License: GPL-2 or LGPL-2.1

Files: CONTRIBUTING
Copyright: header making clear under which license its being released
License: LGPL-2.1

Files: Makefile
Copyright: no-info-found
License: LGPL-2.1

Files: config/*
Copyright: no-info-found
License: LGPL-2.1

Files: config/yum/lxc-patch.py
Copyright: 2012, Oracle
License: LGPL-2.1+

Files: debian/*
Copyright: 2011-2015 Daniel Baumann <mail@daniel-baumann.ch>
            2015-2022 Antonio Terceiro <terceiro@debian.org>
            2016-2017 Evgeni Golov <evgeni@debian.org>
            2018 Salvatore Bonaccorso <carnil@debian.org>
            2018-2022 Pierre-Elliott Bécue <peb@debian.org>
            2022-2023 Mathias Gibbens
License: GPL-2+

Files: doc/*
Copyright: 2007, 2008, IBM Corp.
License: LGPL-2.1+

Files: doc/examples/*
Copyright: no-info-found
License: LGPL-2.1

Files: doc/ja/lxc-autostart.sgml.in
 doc/ja/lxc-checkconfig.sgml.in
 doc/ja/lxc-checkpoint.sgml.in
 doc/ja/lxc-config.sgml.in
 doc/ja/lxc-device.sgml.in
 doc/ja/lxc-info.sgml.in
 doc/ja/lxc-update-config.sgml.in
 doc/ja/lxc-user-nic.sgml.in
 doc/ja/lxc-usernet.sgml.in
 doc/ja/lxc.conf.sgml.in
 doc/ja/lxc.system.conf.sgml.in
Copyright: 2013, 2014, 2016, 2017, Canonical Ltd.
License: LGPL-2.1+

Files: doc/ja/lxc-copy.sgml.in
 doc/ja/lxc-snapshot.sgml.in
 doc/ja/pam_cgfs.sgml.in
Copyright: 2007, 2008, 2019, Canonical Inc.
License: LGPL-2.1+

Files: doc/ja/lxc-top.sgml.in
Copyright: 2012, Oracle.
License: LGPL-2.1+

Files: doc/ja/meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: doc/ko/lxc-autostart.sgml.in
 doc/ko/lxc-checkconfig.sgml.in
 doc/ko/lxc-checkpoint.sgml.in
 doc/ko/lxc-config.sgml.in
 doc/ko/lxc-device.sgml.in
 doc/ko/lxc-info.sgml.in
 doc/ko/lxc-user-nic.sgml.in
 doc/ko/lxc-usernet.sgml.in
 doc/ko/lxc.conf.sgml.in
 doc/ko/lxc.system.conf.sgml.in
Copyright: 2013, 2014, 2016, 2017, Canonical Ltd.
License: LGPL-2.1+

Files: doc/ko/lxc-copy.sgml.in
 doc/ko/lxc-snapshot.sgml.in
Copyright: 2007, 2008, 2019, Canonical Inc.
License: LGPL-2.1+

Files: doc/ko/lxc-top.sgml.in
Copyright: 2012, Oracle.
License: LGPL-2.1+

Files: doc/ko/meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: doc/lxc-autostart.sgml.in
 doc/lxc-checkconfig.sgml.in
 doc/lxc-checkpoint.sgml.in
 doc/lxc-config.sgml.in
 doc/lxc-device.sgml.in
 doc/lxc-info.sgml.in
 doc/lxc-update-config.sgml.in
 doc/lxc-user-nic.sgml.in
 doc/lxc-usernet.sgml.in
 doc/lxc.conf.sgml.in
 doc/lxc.system.conf.sgml.in
Copyright: 2013, 2014, 2016, 2017, Canonical Ltd.
License: LGPL-2.1+

Files: doc/lxc-copy.sgml.in
 doc/lxc-snapshot.sgml.in
 doc/pam_cgfs.sgml.in
Copyright: 2007, 2008, 2019, Canonical Inc.
License: LGPL-2.1+

Files: doc/lxc-top.sgml.in
Copyright: 2012, Oracle.
License: LGPL-2.1+

Files: doc/meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: doc/rootfs/*
Copyright: no-info-found
License: LGPL-2.1

Files: hooks/*
Copyright: 2013, Oracle.
License: GPL-2

Files: hooks/meson.build
Copyright: no-info-found
License: LGPL-2.1

Files: hooks/mountecryptfsroot
Copyright: 2011-2013, Canonical
License: LGPL-2.1+

Files: hooks/nvidia
Copyright: 2017, 2018, NVIDIA CORPORATION.
License: LGPL-2.1+

Files: hooks/squid-deb-proxy-client
Copyright: 2014, Christopher Glass.
License: GPL-2

Files: hooks/unmount-namespace.c
Copyright: 2015, Wolfgang Bumiller <w.bumiller@proxmox.com>.
 2015, Proxmox Server Solutions GmbH
License: GPL-2

Files: lxc.spec.in
Copyright: 2007, 2008, IBM Corp.
License: LGPL-2 and/or LGPL-2.1+

Files: meson.build
Copyright: no-info-found
License: LGPL-2 and/or LGPL-2.1

Files: src/*
Copyright: 2018, Christian Brauner <christian@brauner.io>.
 2018, Canonical Ltd.
License: GPL-2

Files: src/include/bpf.h
Copyright: 2011-2014, PLUMgrid, http://plumgrid.com
License: GPL-2

Files: src/include/bpf_common.h
Copyright: no-info-found
License: GPL-2

Files: src/include/fexecve.c
 src/include/fexecve.h
Copyright: 2019, Christian Brauner <christian.brauner@ubuntu.com>.
 2019, Canonical Ltd.
License: LGPL-2.1+

Files: src/include/getgrgid_r.c
 src/include/getgrgid_r.h
Copyright: 2017-2019, Christian Brauner <christian.brauner@ubuntu.com>.
 2017-2019, Canonical Ltd.
License: GPL-2

Files: src/include/getline.c
 src/include/getline.h
Copyright: 2006, SPARTA, Inc.
License: BSD-2-clause

Files: src/include/lxcmntent.c
 src/include/lxcmntent.h
 src/include/strchrnul.c
 src/include/strchrnul.h
Copyright: 1991-2021, Free Software Foundation, Inc.
License: LGPL-2.1+

Files: src/include/meson.build
 src/include/openpty.c
 src/include/openpty.h
Copyright: no-info-found
License: LGPL-2.1

Files: src/include/prlimit.c
 src/include/prlimit.h
Copyright: 2008, The Android Open Source Project
License: BSD-2-clause

Files: src/lxc/*
Copyright: no-info-found
License: LGPL-2.1

Files: src/lxc/lxc.h
Copyright: lxc_container_put;
License: LGPL-2.1

Files: src/lxc/tools/lxc_checkpoint.c
 src/lxc/tools/lxc_copy.c
 src/lxc/tools/lxc_create.c
 src/lxc/tools/lxc_destroy.c
 src/lxc/tools/lxc_ls.c
 src/lxc/tools/lxc_snapshot.c
Copyright: no-info-found
License: GPL-2

Files: src/tests/*
Copyright: no-info-found
License: LGPL-2.1+

Files: src/tests/aa.c
 src/tests/cgpath.c
 src/tests/clonetest.c
 src/tests/containertests.c
 src/tests/createtest.c
 src/tests/destroytest.c
 src/tests/get_item.c
 src/tests/getkeys.c
 src/tests/locktests.c
 src/tests/lxcpath.c
 src/tests/saveconfig.c
 src/tests/shutdowntest.c
 src/tests/snapshot.c
 src/tests/startone.c
Copyright: 2012-2014, Serge Hallyn <serge.hallyn@ubuntu.com>.
 2012-2014, Canonical Ltd.
License: GPL-2

Files: src/tests/api_reboot.c
 src/tests/arch_parse.c
 src/tests/capabilities.c
 src/tests/console_log.c
 src/tests/rootfs_options.c
 src/tests/share_ns.c
 src/tests/state_server.c
 src/tests/sys_mixed.c
Copyright: 2017, 2021, Christian Brauner <christian.brauner@ubuntu.com>.
License: GPL-2

Files: src/tests/attach.c
 src/tests/console.c
Copyright: 2013, Oracle.
License: GPL-2

Files: src/tests/basic.c
 src/tests/config_jump_table.c
 src/tests/cve-2019-5736.c
 src/tests/parse_config_file.c
 src/tests/shortlived.c
Copyright: 2017-2019, Christian Brauner <christian.brauner@ubuntu.com>.
 2017-2019, Canonical Ltd.
License: GPL-2

Files: src/tests/concurrent.c
 src/tests/device_add_remove.c
Copyright: no-info-found
License: GPL-2

Files: src/tests/criu_check_feature.c
Copyright: 2017, Adrian Reber <areber@redhat.com>
License: GPL-2

Files: src/tests/fuzz-lxc-cgroup-init.c
 src/tests/fuzz-lxc-config-read.c
 src/tests/fuzz-lxc-define-load.c
 src/tests/meson.build
 src/tests/proc_pid.c
 src/tests/sysctls.c
Copyright: no-info-found
License: LGPL-2.1

Files: src/tests/list.c
 src/tests/may_control.c
Copyright: 2013, Canonical, Inc
License: GPL-2

Files: src/tests/lxc-test-utils.c
 src/tests/lxc_raw_clone.c
 src/tests/lxctest.h
Copyright: 2013, 2014, 2016, 2017, Canonical Ltd.
License: LGPL-2.1+

Files: src/tests/mount_injection.c
Copyright: 2018, Elizaveta Tretiakova <elizabet.tretyakova@gmail.com>.
License: GPL-2

Files: src/tests/reboot.c
Copyright: 2012, Serge Hallyn <serge.hallyn@ubuntu.com>.
 2012, Canonical Ltd.
License: LGPL-2.1+

Files: templates/*
Copyright: no-info-found
License: LGPL-2.1

Files: templates/lxc-busybox.in
Copyright: 2018, Christian Brauner <christian.brauner@ubuntu.com>
License: LGPL-2.1+

Files: templates/lxc-download.in
Copyright: 2014, Stéphane Graber <stgraber@ubuntu.com>
License: LGPL-2.1+

Files: templates/lxc-local.in
Copyright: 2018, Stéphane Graber <stgraber@ubuntu.com>
 2018, Christian Brauner <christian.brauner@ubuntu.com>
License: LGPL-2.1+

Files: templates/lxc-oci.in
Copyright: 2017, Serge Hallyn <serge@hallyn.com>
 2014, Stéphane Graber <stgraber@ubuntu.com>
License: LGPL-2.1+
